package dhbw.web.proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.WebRequest;

import dhbw.web.interfaces.service.AnswerService;
import dhbw.web.interfaces.service.AuthenticationService;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.FollowService;
import dhbw.web.interfaces.service.LikeService;
import dhbw.web.interfaces.service.QuestionService;
import dhbw.web.interfaces.service.SearchService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.to.mapper.ModelTOMapper;
import dhbw.web.service.AnswerServiceImpl;
import dhbw.web.service.AuthServiceImpl;
import dhbw.web.service.CommentServiceImpl;
import dhbw.web.service.FollowServiceImpl;
import dhbw.web.service.LikeServiceImpl;
import dhbw.web.service.QuestionServiceImpl;
import dhbw.web.service.SearchServiceImpl;
import dhbw.web.service.UserServiceImpl;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "dhbw.web.proxy", "dhbw.web.service" })
@Slf4j
public class ProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProxyApplication.class, args);
	}

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		log.debug("get rest");
		return new RestTemplate();
	}

	@Bean
	public MapperWrapper mapper() {
		log.debug("get mapper to");
		return ModelTOMapper.getModelToMapper();
	}

	@Bean
	public UserService userService() {
		log.debug("1: user service");
		UserService us = new UserServiceImpl();
		log.debug("1: user service constructed");
		return us;
	}

	@Bean
	public QuestionService questionService() {
		log.debug("3: question service");
		QuestionService qs = new QuestionServiceImpl();
		log.debug("3: question service constructed");
		return qs;
	}

	@Bean
	public AnswerService answerService() {
		log.debug("5: answer service");
		AnswerService as = new AnswerServiceImpl();
		log.debug("5: answer service constructed");
		return as;
	}

	@Bean
	public CommentService commentService() {
		log.debug("7: comment service");
		CommentService cs = new CommentServiceImpl();
		log.debug("7: comment service constructed");
		return cs;
	}

	@Bean
	public AuthenticationService authService() {
		log.debug("9: auth service");
		AuthenticationService as = new AuthServiceImpl();
		log.debug("9: auth service constructed");
		return as;
	}

	@Bean
	public FollowService followService() {
		log.debug("11: follow service");
		FollowService fs = new FollowServiceImpl();
		log.debug("11: follow service constructed");
		return fs;
	}

	@Bean
	public SearchService searchService() {
		log.debug("13: search service");
		SearchService ss = new SearchServiceImpl();
		log.debug("13: search service constructed");
		return ss;
	}

	@Bean
	public LikeService likeService() {
		log.debug("15: follow service");
		LikeService ls = new LikeServiceImpl();
		log.debug("15: follow service constructed");
		return ls;
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
		log.error(buildError(ex, 0, new StringBuilder()).toString());
		return new ResponseEntity<Object>(ex, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private StringBuilder buildError(Throwable ex, int index, StringBuilder sb) {
		sb.append(String.format("%d\t: name: %s\n\nmessage: %s\n\nstacktrace: %s\n\n", index, ex.getClass().getName(),
				ex.getMessage(), ex.getStackTrace()));
		if (ex.getCause() == null) {
			return sb;
		}
		return buildError(ex.getCause(), ++index, sb);
	}

}
