package dhbw.web.proxy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.AuthenticationService;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "/api/auth")
@RequiredArgsConstructor
public class AuthController {

	@NonNull
	private AuthenticationService authService;
}
