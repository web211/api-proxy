package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.model.to.NotificationTO;
import dhbw.web.utils.MapperWrapper;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/notify")
@RequiredArgsConstructor
@Slf4j
public class NotificationController {

	@NonNull
	private NotificationService notifyService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@GetMapping("/{id}/latest")
	public List<NotificationTO> getLatestUserNotifications(@PathVariable("id") int id,
			@RequestParam(name = "amount", required = false, defaultValue = "10") int amount) {
		log.debug(String.format("read all latest notifications for user %d with the quontity %d", id, amount));
		return this.mapper.map(this.notifyService.getLatestUserNotifications(id, amount), NotificationTO.class);
	}

	@LoadBalanced
	@GetMapping("/{id}")
	public List<NotificationTO> getAllUserNotifications(@PathVariable("id") int id) {
		log.debug("read all notifications of user with id: " + id);
		return this.mapper.map(this.notifyService.getAllUserNotifications(id), NotificationTO.class);
	}

}
