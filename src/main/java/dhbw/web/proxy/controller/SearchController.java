package dhbw.web.proxy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.SearchService;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/search")
@RequiredArgsConstructor
public class SearchController {

	@NonNull
	private SearchService searchService;

}
