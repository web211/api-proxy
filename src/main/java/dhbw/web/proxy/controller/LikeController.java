package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.LikeService;
import dhbw.web.model.to.LikeTO;
import dhbw.web.utils.MapperWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/like")
@RequiredArgsConstructor
@Slf4j
public class LikeController {

	@NonNull
	private LikeService likeService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@GetMapping(path = "/{type}/{id}")
	public List<LikeTO> readLike(@PathVariable("id") int element, @PathVariable("type") String type,
			@RequestParam(required = false, name = "userId", defaultValue = "0") int userId) {
		log.debug(String.format("read like of element with id %d, of type %s. The passed user id is %d", element, type,
				userId));
		return this.mapper.map(this.likeService.readLike(element, type, userId), LikeTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "")
	public List<LikeTO> readAllLikes() {
		log.debug("read all likes which exists");
		return this.mapper.map(this.likeService.readAllLikes(), LikeTO.class);
	}

}
