package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.Commentable;
import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.Followable;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.FollowService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.User;
import dhbw.web.model.form.CreateCommentForm;
import dhbw.web.model.form.ToggleFollowForm;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.FollowTO;
import dhbw.web.model.to.UserTO;
import dhbw.web.utils.MapperWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

	@NonNull
	protected UserService userService;

	@NonNull
	protected FollowService followService;

	@NonNull
	private CommentService commentService;

	@Autowired
	protected MapperWrapper mapper;

	@LoadBalanced
	@PostMapping(path = "")
	public UserTO createUser(@RequestParam String username, @RequestParam String email,
			@RequestParam(required = false, defaultValue = "") String firstname,
			@RequestParam(required = false, defaultValue = "") String lastname, @RequestParam String password) {
		UserTO user = new UserTO();
		user.setUsername(username);
		user.setEmail(email);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setPassword(password);
		log.debug("create user with the following parameters:  " + user);
		return mapper.map(this.userService.createUser(user), UserTO.class);
	}

	@LoadBalanced
	@GetMapping("")
	public List<UserTO> readAllUser() {
		log.debug("read all users");
		return this.mapper.map(this.userService.readAllUser(), UserTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}")
	public UserTO readUser(@PathVariable("id") int id) {
		log.debug("read user by id: " + id);
		return mapper.map(this.userService.readUser(id), UserTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/name/{username}")
	public UserTO readUser(@PathVariable("username") String username) {
		log.debug("read user by username: " + username);
		return mapper.map(this.userService.readUserByUsername(username), UserTO.class);
	}

	@LoadBalanced
	@PutMapping(path = "/{id}")
	public void updateUser(@PathVariable int id, @RequestParam(required = false, defaultValue = "") String email,
			@RequestParam(required = false, defaultValue = "") String firstname,
			@RequestParam(required = false, defaultValue = "") String lastname,
			@RequestParam(required = false, defaultValue = "") String password) {
		UserTO user = new UserTO();
		user.setId(id);
		user.setEmail(email);
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setPassword(password);
		log.debug("update the set user fields: " + user);
		this.userService.updateUser(user);
	}

	@LoadBalanced
	@DeleteMapping(path = "/{id}")
	public void deleteUser(@PathVariable("id") int id) {
		log.debug("delete user by id: " + id);
		this.userService.deleteUser(id);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/follow")
	public List<FollowTO> readElementFollower(@PathVariable int id,
			@RequestParam(required = false, defaultValue = "0") int userId) {
		if (userId == 0) {
			log.debug("read all follower which follow the user with id: " + id);
		} else {
			log.debug(String.format("read follower with id %d from user with id %d", userId, id));
		}
		return this.mapper.map(this.followService.readFollow(id, User.TYPE, userId), FollowTO.class);
	}

	@LoadBalanced
	@PutMapping(path = "/{id}/follow")
	public FollowTO toggleFollow(@PathVariable int id, @RequestParam int userId) {
		Followable user = this.userService.readUser(id);
		User follower = this.userService.readUser(userId);
		ToggleFollowForm form = new ToggleFollowForm();
		form.setElement(user);
		form.setFollower(follower);
		log.debug(String.format("toggle follow of user with id %d for user with id %d", userId, id));
		return this.mapper.map(this.followService.toggleFollow(form), FollowTO.class);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/comment")
	public CommentTO addComment(@PathVariable int id, @RequestParam String text, @RequestParam int userId) {
		User user = this.userService.readUser(userId);
		Commentable question = this.userService.readUser(id);
		CommentTO comment = new CommentTO();
		comment.setText(text);
		CreateCommentForm form = new CreateCommentForm();
		form.setComment(comment);
		form.setParent(question);
		form.setUser(user);
		log.debug(String.format("add comment to user with id %d and the text '%s' from user %d", id, text, userId));
		return this.mapper.map(this.commentService.createComment(form), CommentTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/comment")
	public List<CommentTO> getAllCommentsOfUser(@PathVariable int id) {
		ElementParent question = this.userService.readUser(id);
		log.debug("read all comments for user with id: " + id);
		return this.mapper.map(this.commentService.getElemnetComments(question.getId(), question.getType()),
				CommentTO.class);
	}

}
