package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.Likeable;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.LikeService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.Comment;
import dhbw.web.model.User;
import dhbw.web.model.form.ToggleLikeForm;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.LikeTO;
import dhbw.web.utils.MapperWrapper;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/comment")
@RequiredArgsConstructor
@Slf4j
public class CommentController {

	@NonNull
	private CommentService commentService;

	@NonNull
	private UserService userService;

	@NonNull
	private LikeService likeService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@PutMapping("/{id}")
	public void updateComment(@PathVariable int id, @RequestParam String text, @RequestParam int userId) {
		CommentTO comment = new CommentTO();
		comment.setId(id);
		comment.setText(text);
		comment.setUserId(userId);
		log.debug("create comment: " + comment);
		this.commentService.updateComment(comment);
	}

	@LoadBalanced
	@GetMapping("{id}")
	public CommentTO readComment(@PathVariable("id") int id) {
		log.debug("read comment with id: " + id);
		return this.mapper.map(this.commentService.readComment(id), CommentTO.class);
	}

	@LoadBalanced
	@DeleteMapping("{id}")
	public void deleteComment(@PathVariable("id") int id) {
		log.debug("delete comment with id: " + id);
		this.commentService.deleteComment(id);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/like")
	public LikeTO readCommentLikes(@PathVariable(name = "id") int id, @RequestParam(name = "userId") int userId) {
		User user = this.userService.readUser(userId);
		Likeable comment = this.commentService.readComment(id);
		ToggleLikeForm form = new ToggleLikeForm();
		form.setElement(comment);
		form.setLiked(user);
		log.debug(String.format("toggle like from user with id %d for comment with id %d", userId, id));
		return this.mapper.map(this.likeService.toggleLike(form), LikeTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/like")
	public List<LikeTO> readCommentLikes(@PathVariable(name = "id") int id) {
		log.debug("read all likes from comment with id: " + id);
		return this.mapper.map(this.likeService.readLike(id, Comment.TYPE, 0), LikeTO.class);
	}
}
