package dhbw.web.proxy.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.Likeable;
import dhbw.web.interfaces.service.AnswerService;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.LikeService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.Answer;
import dhbw.web.model.User;
import dhbw.web.model.form.CreateCommentForm;
import dhbw.web.model.form.ToggleLikeForm;
import dhbw.web.model.to.AnswerTO;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.LikeTO;
import dhbw.web.utils.MapperWrapper;
import io.micrometer.core.lang.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/answer")
@RequiredArgsConstructor
@Slf4j
public class AnswerController {

	@NonNull
	private AnswerService answerService;

	@NonNull
	private CommentService commentService;

	@NonNull
	private UserService userService;

	@NonNull
	private LikeService likeService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@PutMapping(path = "/{id}")
	public void updateAnswer(@PathVariable("id") int id, @RequestParam("text") String text,
			@RequestParam("userId") int userId) {
		AnswerTO answer = new AnswerTO();
		answer.setId(id);
		answer.setText(text);
		answer.setUserId(id);
		answer.setUserId(userId);
		log.debug(String.format("update answer with id %d, new text is '%s' and the update is done by userid %d", id,
				text, userId));
		this.answerService.updateAnswer(answer);
	}

	@LoadBalanced
	@DeleteMapping("/{id}")
	public void deleteAnswer(@PathVariable("id") int id) {
		log.debug("delete answer with id: " + id);
		this.answerService.deleteAnswer(id);
	}

	@LoadBalanced
	@GetMapping("/{id}")
	public AnswerTO readAnswer(@PathVariable("id") int id) {
		log.debug("read answer with id: " + id);
		return this.mapper.map(this.answerService.readAnswer(id), AnswerTO.class);
	}

	@LoadBalanced
	@PostMapping("/{id}/comment")
	public CommentTO createComment(@PathVariable("id") int id, @RequestParam("text") String text,
			@RequestParam("userId") int userId) {
		User user = this.userService.readUser(userId);
		Answer answer = this.answerService.readAnswer(id);
		CommentTO comment = new CommentTO();
		comment.setText(text);
		CreateCommentForm form = new CreateCommentForm();
		form.setUser(user);
		form.setParent(answer);
		form.setComment(comment);
		log.debug(
				String.format("create comment for answer with id %d with the text '%s' by user %d", id, text, userId));
		return this.mapper.map(this.commentService.createComment(form), CommentTO.class);
	}

	@LoadBalanced
	@GetMapping("/{id}/comment")
	public List<CommentTO> getAllCommentsOfAnswer(@PathVariable("id") int id) {
		Answer answer = this.answerService.readAnswer(id);
		log.debug("read comments of the answer with id: " + id);
		return answer.getAllComments().stream().map(x -> this.mapper.map(x, CommentTO.class))
				.collect(Collectors.toList());
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/like")
	public LikeTO readAnswerLikes(@PathVariable(name = "id") int id, @RequestParam(name = "userId") int userId) {
		User user = this.userService.readUser(userId);
		Likeable answer = this.answerService.readAnswer(id);
		ToggleLikeForm form = new ToggleLikeForm();
		form.setElement(answer);
		form.setLiked(user);
		log.debug(String.format("toggle like from user with id %d for answer with id %d", user, id));
		return this.mapper.map(this.likeService.toggleLike(form), LikeTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/like")
	public List<LikeTO> readAnswerLikes(@PathVariable(name = "id") int id) {
		log.debug("read all likes from answer with id: " + id);
		return this.mapper.map(this.likeService.readLike(id, Answer.TYPE, 0), LikeTO.class);
	}
}
