package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.Followable;
import dhbw.web.interfaces.Likeable;
import dhbw.web.interfaces.service.AnswerService;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.FollowService;
import dhbw.web.interfaces.service.LikeService;
import dhbw.web.interfaces.service.QuestionService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.Answer;
import dhbw.web.model.Follow;
import dhbw.web.model.Question;
import dhbw.web.model.User;
import dhbw.web.model.form.CreateAnswerForm;
import dhbw.web.model.form.CreateCommentForm;
import dhbw.web.model.form.CreateQuestionForm;
import dhbw.web.model.form.ToggleFollowForm;
import dhbw.web.model.form.ToggleLikeForm;
import dhbw.web.model.to.AnswerTO;
import dhbw.web.model.to.CommentTO;
import dhbw.web.model.to.FollowTO;
import dhbw.web.model.to.LikeTO;
import dhbw.web.model.to.QuestionTO;
import dhbw.web.utils.MapperWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/api/question")
@RequiredArgsConstructor
@Slf4j
public class QuestionController {

	@NonNull
	private QuestionService questionService;

	@NonNull
	private AnswerService answerService;

	@NonNull
	private CommentService commentService;

	@NonNull
	private UserService userService;

	@NonNull
	private FollowService followService;

	@NonNull
	private LikeService likeService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@GetMapping(path = "")
	public List<QuestionTO> getAllQuestions(@RequestParam(required = false, defaultValue = "0") int id,
			@RequestParam(required = false, defaultValue = "") String url) {
		if (id != 0) {
			log.debug("read question by id: " + id);
			return this.mapper.map(new Question[] { this.questionService.readQuestion(id) }, QuestionTO.class);
		} else if (url.length() > 0) {
			log.debug("read question by url: " + url);
			return this.mapper.map(new Question[] { this.questionService.readQuestionByUrl(url) }, QuestionTO.class);
		} else {
			log.debug("read all questions");
			return this.mapper.map(this.questionService.getAllQuestions(), QuestionTO.class);
		}
	}

	@LoadBalanced
	@PostMapping(path = "")
	public QuestionTO createQuestion(@RequestParam String title,
			@RequestParam(required = false, defaultValue = "") String url, @RequestParam String text,
			@RequestParam int userId) {
		User user = this.userService.readUser(userId);
		QuestionTO question = new QuestionTO();
		question.setTitle(title);
		question.setUrl(url);
		question.setText(text);
		question.setUserId(userId);
		CreateQuestionForm form = new CreateQuestionForm();
		form.setQuestion(question);
		form.setUser(user);
		log.debug("create question by form: " + form);
		return this.mapper.map(this.questionService.createQuestion(form), QuestionTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}")
	public QuestionTO readQuestion(@PathVariable int id) {
		log.debug("read question by id: " + id);
		return this.mapper.map(this.questionService.readQuestion(id), QuestionTO.class);
	}

	@LoadBalanced
	@PutMapping(path = "/{id}")
	public void updateQuestion(@PathVariable int id, @RequestParam(required = false, defaultValue = "") String url,
			@RequestParam(required = false, defaultValue = "") String title,
			@RequestParam(required = false, defaultValue = "") String text, @RequestParam int userId) {
		QuestionTO question = new QuestionTO();
		question.setId(id);
		question.setTitle(title);
		question.setUrl(url);
		question.setText(text);
		question.setUserId(userId);
		log.debug("update question by object: " + question);
		this.questionService.updateQuestion(question);
	}

	@LoadBalanced
	@DeleteMapping(path = "/{id}")
	public void deleteQuestion(@PathVariable int id) {
		log.debug("delete question by id: " + id);
		this.questionService.deleteQuestion(id);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/answer")
	public AnswerTO addAnswer(@PathVariable int id, @RequestParam String text, @RequestParam int userId) {
		User user = this.userService.readUser(userId);
		Question question = this.questionService.readQuestion(id);
		AnswerTO answer = new AnswerTO();
		answer.setText(text);
		CreateAnswerForm form = new CreateAnswerForm();
		form.setAnswer(answer);
		form.setParent(question);
		form.setUser(user);
		log.debug(String.format(
				"add answer to question with id %d, answer text is '%s' and it is created by user with id %d", id, text,
				userId));
		return this.mapper.map(this.answerService.createAnswer(form), AnswerTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/answer")
	public List<AnswerTO> getAllAnswersOfQuestion(@PathVariable int id) {
		Answer[] answers = this.answerService.getQuestionAnswers(id);
		log.debug("read all answers from question with id: " + id);
		return this.mapper.map(answers, AnswerTO.class);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/comment")
	public CommentTO addComment(@PathVariable int id, @RequestParam String text, @RequestParam int userId) {
		User user = this.userService.readUser(userId);
		Question question = this.questionService.readQuestion(id);
		CommentTO comment = new CommentTO();
		comment.setText(text);
		CreateCommentForm form = new CreateCommentForm();
		form.setComment(comment);
		form.setParent(question);
		form.setUser(user);
		log.debug(String.format(
				"add comment to question with id %d, answer text is '%s' and it is created by user with id %d", id,
				text, userId));
		return this.mapper.map(this.commentService.createComment(form), CommentTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/comment")
	public List<CommentTO> getAllCommentsOfQuestion(@PathVariable int id) {
		ElementParent question = this.questionService.readQuestion(id);
		log.debug("read all comments from question with id: " + id);
		return this.mapper.map(this.commentService.getElemnetComments(question.getId(), question.getType()),
				CommentTO.class);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/follow")
	public FollowTO toggleQuestionFollow(@PathVariable int id, @RequestParam int userId) {
		User user = this.userService.readUser(userId);
		Followable question = this.questionService.readQuestion(id);
		ToggleFollowForm form = new ToggleFollowForm();
		form.setElement(question);
		form.setFollower(user);
		log.debug(String.format("toggle follow from user with id %d for question with id %d", userId, id));
		Follow f = this.followService.toggleFollow(form);
		return this.mapper.map(f, FollowTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/follow")
	public List<FollowTO> readQuestionFollows(@PathVariable int id) {
		log.debug("read all follower from question with id: " + id);
		return this.mapper.map(this.followService.readFollow(id, Question.TYPE, 0), FollowTO.class);
	}

	@LoadBalanced
	@PostMapping(path = "/{id}/like")
	public LikeTO readQuestionLikes(@PathVariable int id, @RequestParam int userId) {
		User user = this.userService.readUser(userId);
		Likeable question = this.questionService.readQuestion(id);
		ToggleLikeForm form = new ToggleLikeForm();
		form.setElement(question);
		form.setLiked(user);
		log.debug(String.format("toggle like from user with id %d for question with id %d", userId, id));
		return this.mapper.map(this.likeService.toggleLike(form), LikeTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "/{id}/like")
	public List<LikeTO> readQuestionLikes(@PathVariable int id) {
		log.debug("read all like from question with id: " + id);
		return this.mapper.map(this.likeService.readLike(id, Question.TYPE, 0), LikeTO.class);
	}

}
