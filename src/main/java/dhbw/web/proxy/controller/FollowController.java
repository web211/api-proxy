package dhbw.web.proxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.FollowService;
import dhbw.web.model.to.FollowTO;
import dhbw.web.utils.MapperWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/follow")
@RequiredArgsConstructor
@Slf4j
public class FollowController {

	@NonNull
	private FollowService followService;

	@Autowired
	private MapperWrapper mapper;

	@LoadBalanced
	@GetMapping(path = "/{type}/{id}")
	public List<FollowTO> readFollow(@PathVariable("id") int element, @PathVariable("type") String type,
			@RequestParam(required = false, name = "userId", defaultValue = "0") int userId) {
		log.debug(String.format("read follower of element with id %d, of type %s. The passed user id is %d", element,
				type, userId));
		return this.mapper.map(this.followService.readFollow(element, type, userId), FollowTO.class);
	}

	@LoadBalanced
	@GetMapping(path = "")
	public List<FollowTO> readAllFollower() {
		log.debug("read all follows which exists");
		return this.mapper.map(this.followService.readAllFollower(), FollowTO.class);
	}

}
